const puppeteer = require('puppeteer');
const express = require('express');
const config = require('./config.json');
const rooms = {};

(async () => {
    const browser = await puppeteer.launch({
        ignoreHTTPSErrors: true,
        args: [
            // Required for Docker version of Puppeteer
            '--no-sandbox',
            '--disable-setuid-sandbox',
            // This will write shared memory files into /tmp instead of /dev/shm,
            // because Docker’s default for /dev/shm is 64MB
            '--disable-dev-shm-usage'
        ],
    });
    const app = express();
    app.use(express.json());

    app.post('/rooms', async (req, res) => {
        const { roomName, displayName } = req.body;

        if (rooms[roomName]) {
            res.status(404).json({ error: 'Already connected to room' });
            return;
        }

        const baseURL = (config.allowCustomBaseURLs && req.body.baseURL) || config.baseURL;
        const avatarURL = (config.allowCustomAvatarURLs && req.body.avatarURL) || config.avatarURL;
        const page = await browser.newPage();
        page.setContent('<body><div id="jitsiContainer"></div></body>');
        await page.addScriptTag({ path: './external_api.js' });
        await page.evaluate(
            ({ baseURL, roomName, displayName, avatarURL }) => {
                window.api = new JitsiMeetExternalAPI(
                    baseURL,
                    {
                        roomName: roomName,
                        parentNode: document.querySelector('#jitsiContainer'),
                        configOverwrite: {
                            prejoinPageEnabled: false,
                        },
                    },
                );

                window.api.addEventListener('videoConferenceJoined', () => {
                    displayName && window.api.executeCommand('displayName', displayName);
                    avatarURL && window.api.executeCommand('avatarUrl', avatarURL);
                });
            },
            { baseURL, roomName, displayName, avatarURL },
        );
        rooms[roomName] = page;
        res.status(200).json({ status: "ok" });
    });

    app.get('/rooms/:roomId/numberofparticipants', async (req, res) => {
        const { roomId } = req.params;
        if (roomId in rooms) {
            res.json({ data: await rooms[roomId].evaluate(() => window.api.getNumberOfParticipants()) });
        }
        else {
            res.status(404).json({ error: 'Room does not exist' });
        }
    });

    app.get('/rooms/:roomId/participantsinfo', async (req, res) => {
        const { roomId } = req.params;
        if (roomId in rooms) {
            res.json({ data: await rooms[roomId].evaluate(() => window.api.getParticipantsInfo()) });
        }
        else {
            res.status(404).json({ error: 'Room does not exist' });
        }
    });

    app.get('/rooms/:roomId/leave', async (req, res) => {
        const { roomId } = req.params;
        if (roomId in rooms) {
            await rooms[roomId].close();
            delete rooms[roomId];
            res.status(200).json({ status: "ok" });
        }
        else {
            res.status(404).json({ error: 'Room does not exist' });
        }
    });

    app.listen(config.port, async () => {
        console.log(`Jitsi app listening at http://localhost:${config.port}`);
    });
})().catch(error => console.log(error));
